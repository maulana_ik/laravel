<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/form', function(){
    return view('form');
});

//Route::post('/welcome to', 'SignController@welcome');
//Route::post('/welcome1' function(){
    //return view('welcome1')
//})

Route::post('/welcome1', 'ConfigController@welcome');